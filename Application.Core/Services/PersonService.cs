﻿using Application.Infrastructure;
using Application.Validators;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace Application.Services
{
    public class PersonService : IPersonService
    {
        protected readonly IUnitOfWork _unitOfWork;
        protected readonly ILogger _logger;
        protected readonly IValidator _validator;

        public PersonService(IUnitOfWork unitOfWork, ILogger<PersonService> logger, IValidator validator)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _validator = validator;
        }

        public Person GetPerson(long personId)
        {
            var person = _unitOfWork.Query<Person>().Where(e=> e.Id == personId).ToList().FirstOrDefault();
            if (person == null)
            {
                var message = $"There is no person with id: {personId}";
                _logger.LogWarning(message);
                throw new ApplicationException(message);
            }

            return person;
        }

        public void Validate()
        {
            var person = new Person()
            {
                Id = 1,
                Name = "Lucas"
            };
            _validator.Validate<PersonValidator>(person, true);
        }
    }
}