﻿using Application.Infrastructure;

namespace Application.Services
{
    public interface IPersonService
    {
        Person GetPerson(long personId);
    }
}