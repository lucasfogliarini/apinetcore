﻿using Application.Infrastructure;
using FluentValidation;

namespace Application.Validators
{
    public class PersonValidator : AbstractValidator<Person>
    {
        public PersonValidator()
        {
            RuleFor(i => i.Id).NotEmpty();
            RuleFor(i => i.Name).NotEmpty();
        }
    }
}
