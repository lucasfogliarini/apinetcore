﻿using FluentValidation.Results;

namespace Application.Validators
{
    public interface IValidator
    {
        ValidationResult Validate<TValidator>(object entity, bool throwIfNotValid = false) where TValidator : FluentValidation.IValidator;
    }
}
