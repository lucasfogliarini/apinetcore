﻿using Application.Validators;
using Microsoft.Extensions.DependencyInjection;

namespace Application.Services
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddTransient<IPersonService, PersonService>();
            return services;
        }
        public static IServiceCollection AddValidator(this IServiceCollection services)
        {
            services.AddSingleton<IValidator, Validator>();
            return services;
        }
    }
}