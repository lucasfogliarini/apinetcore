﻿using Application.Infrastructure;
using Application.Services;
using Application.Tests.Unit;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using Xunit;

namespace Application.Tests.Unit
{
    public class PersonServiceTests : BaseTest
    {
        private readonly IPersonService _personService;

        public PersonServiceTests()
        {
            _personService = ServiceProvider.GetService<IPersonService>();
        }

        [Fact]
        public void GetPerson_Should_ReturnNotNullPerson()
        {
            var personId = 1;
            SeedInvoice(personId);

            var person = _personService.GetPerson(personId);

            Assert.NotNull(person);
        }

        private void SeedInvoice(long personid)
        {
            var unitOfWork = ServiceProvider.GetService<IUnitOfWork>();
            unitOfWork.Add(new Person()
            {
                Id = personid,
                Name = "Lucas"
            });
        }
    }
}
