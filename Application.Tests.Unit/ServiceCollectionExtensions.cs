﻿using Application.Infrastructure;
using Microsoft.Extensions.DependencyInjection;

namespace Application.Tests.Unit
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddFakeUnitOfWork(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, FakeUnitOfWork>();
            return services;
        }
    }
}