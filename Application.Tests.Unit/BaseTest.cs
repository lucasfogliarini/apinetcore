﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Application.Services;
using Application.Infrastructure;
using Newtonsoft.Json;
using Xunit;

namespace Application.Tests.Unit
{
    public class BaseTest
    {
        protected ServiceProvider ServiceProvider { get; set; }
        public BaseTest()
        {
            ConfigureServiceCollection();   
        }

        private void ConfigureServiceCollection()
        {
            var serviceCollection = new ServiceCollection();
            AddUnitOfWork(serviceCollection);
            serviceCollection.AddServices();
            serviceCollection.AddValidator();
            serviceCollection.AddLogging();
            ServiceProvider = serviceCollection.BuildServiceProvider();
        }

        private void AddUnitOfWork(ServiceCollection services)
        {
            var appSettings = new AppSettingsTest();
            if (appSettings.UseMock)
            {
                services.AddFakeUnitOfWork();
            }
            else
            {
                services.AddApplicationUnitOfWork(appSettings.ConnectionString);
            }
        }

        protected void Equivalent(object expected, object atual)
        {
            var expectedStr = JsonConvert.SerializeObject(expected);
            var actualStr = JsonConvert.SerializeObject(atual);

            Assert.Equal(expectedStr, actualStr);
        }
    }
}
