﻿namespace Application.Tests.Unit
{
    public class AppSettingsTest
    {
        public string ConnectionString { get; } = "connectionStringTest";
        public bool UseMock { get; } = true;
    }
}
