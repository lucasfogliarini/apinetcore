﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Infrastructure
{
    public abstract class UnitOfWork : IUnitOfWork
    {
        protected DbContext DbContext { get; private set; }
        public UnitOfWork(DbContext dbContext)
        {
            DbContext = dbContext;
        }

        public IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntity
        {
            return DbContext.Set<TEntity>().AsNoTracking();
        }

        public void Add<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            DbContext.Add(entity);
        }

        public virtual void Commit()
        {
            DbContext.SaveChanges();
        }

        public void Update<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            DbContext.Update(entity);
        }

        public async Task<int> ExecuteSqlCommandAsync(string rawSqlString, params object[] parameters)
        {
            return await DbContext.Database.ExecuteSqlCommandAsync(rawSqlString, parameters);
        }

        public int ExecuteSqlCommand(string rawSqlString, params object[] parameters)
        {
            return DbContext.Database.ExecuteSqlCommand(rawSqlString, parameters);
        }

        public void Dispose()
        {
            DbContext.Dispose();
        }        
    }
}
