﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Infrastructure
{
    public interface IUnitOfWork : IDisposable
    {
        IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntity;
        void Add<TEntity>(TEntity entity) where TEntity : class, IEntity;
        void Update<TEntity>(TEntity entity) where TEntity : class, IEntity;
        void Commit();
        int ExecuteSqlCommand(string rawSqlString, params object[] parameters);
        Task<int> ExecuteSqlCommandAsync(string rawSqlString, params object[] parameters);
    }
}
