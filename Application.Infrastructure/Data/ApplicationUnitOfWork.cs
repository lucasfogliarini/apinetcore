﻿using Microsoft.EntityFrameworkCore;
using System;

namespace Application.Infrastructure
{
    internal class ApplicationUnitOfWork : UnitOfWork
    {
        public ApplicationUnitOfWork(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public override void Commit()
        {
            try
            {
                DbContext.SaveChanges();
            }
            catch (Exception ex) when (ex.InnerException != null)
            {
                throw new ApplicationException(ex.InnerException.Message, ex);
            }
        }
    }
}
