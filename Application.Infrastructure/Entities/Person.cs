﻿namespace Application.Infrastructure
{
    public class Person : IEntity
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}