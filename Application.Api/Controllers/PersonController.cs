﻿using Application.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Application.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : BaseController
    {
        private readonly IPersonService _personService;
        public PersonController(IPersonService personService)
        {
            _personService = personService;
        }

        [Authorize, HttpGet("{personId}")]
        public IActionResult Get(long personId)
        {
            return TryProcess(() =>
            {
                var person = _personService.GetPerson(personId);
                return Ok(person);
            });
        }
    }
}
