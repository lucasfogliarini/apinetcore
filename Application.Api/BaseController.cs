﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Application.Api.Controllers
{
    public abstract class BaseController : ControllerBase
    {
        protected IActionResult TryProcess(Func<IActionResult> process)
        {
            try
            {
                return process();
            }
            catch (ApplicationException ex)
            {
                return this.BadRequest(ex);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, ex);
            }
        }
    }
}
