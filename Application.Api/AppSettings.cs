﻿namespace Application.Api
{
    public class AppSettings
    {
        public string ConnectionString { get; set; }
        public string LoggingServerUrl { get; set; }
        public bool UseAuthentication { get; set; }
        public string AuthKey { get; set; }
    }
}
