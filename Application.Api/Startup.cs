﻿using Application.Infrastructure;
using Application.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;


namespace Application.Api
{
    public class Startup
    {
        const string APP_NAME = "ApiNetCore";
        public IConfiguration Configuration { get; }
        public IHostingEnvironment Environment { get; }
        AppSettings AppSettings { get; set; }

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            Environment = env;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            AppSettings = AddAppSettings(services);
            AddUnitOfWork(services);
            AddLogging(services);
            services.AddServices();
            services.AddValidator();
            AddMvc(services);
            AddSwaggerGen(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            UseAuthentication(app);

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", APP_NAME);
            });

            app.UseMvc();//end
        }

        private void UseAuthentication(IApplicationBuilder app)
        {
            if (AppSettings.UseAuthentication)
            {
                app.UseAuthentication();
                app.Map("/api/token", (config) =>
                {
                    config.Run(async context =>
                    {
                        JwtSecurityToken jwtToken = new JwtSecurityToken
                        (
                            signingCredentials: new SigningCredentials(GetSecurityKey(), SecurityAlgorithms.HmacSha256),
                            expires: DateTime.UtcNow.AddHours(1)
                        );
                        var tokenString = new JwtSecurityTokenHandler().WriteToken(jwtToken);
                        await context.Response.WriteAsync(tokenString);
                    });
                });
            }
        }

        private void AddLogging(IServiceCollection services)
        {
            services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.AddDebug();
                if (!string.IsNullOrEmpty(AppSettings.LoggingServerUrl))
                {
                    loggingBuilder.AddSeq(AppSettings.LoggingServerUrl);
                }
            });
        }

        private AppSettings AddAppSettings(IServiceCollection services)
        {
            if (Environment.IsDevelopment())
            {
                AppSettings appSettings = Configuration.GetSection(nameof(AppSettings)).Get<AppSettings>();
                services.AddSingleton(appSettings);
                return appSettings;
            }
            else
            {
                //configure host settings integration
                AppSettings appSettings = Configuration.GetSection(nameof(AppSettings)).Get<AppSettings>();
                services.AddSingleton(appSettings);
                return appSettings;
            }
        }

        private void AddUnitOfWork(IServiceCollection services)
        {
            services.AddApplicationUnitOfWork(AppSettings.ConnectionString);
        }

        private void AddSwaggerGen(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = APP_NAME, Version = "v1" });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Enter into 'Value': Bearer {token}",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> {
                    { "Bearer", new string[0] }
                });
            });
        }

        private void AddMvc(IServiceCollection services)
        {
            if (AppSettings.UseAuthentication)
            {
                AddAuthentication(services);
                services.AddMvc(options =>
                {
                    var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();
                    options.Filters.Add(new AuthorizeFilter(policy));
                });
            }
            else
            {
                services.AddMvc();
            }
        }

        private void AddAuthentication(IServiceCollection services)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(x =>
            {
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    IssuerSigningKey = GetSecurityKey(),
                    ValidateIssuerSigningKey = true,
                    ValidateLifetime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
        }

        private SecurityKey GetSecurityKey()
        {
            var key = Encoding.UTF8.GetBytes(AppSettings.AuthKey);
            return new SymmetricSecurityKey(key);
        }
    }
}
